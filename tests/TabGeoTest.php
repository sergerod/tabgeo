<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2016
 * @license MIT
 */

use SergeRod\TabGeo\TabGeo;

class TabGeoTest extends \PHPUnit_Framework_TestCase
{
    protected $filename;

    protected function setUp()
    {
        $this->filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'tabgeo_country_v4.dat';
    }


    public function testCountryUS()
    {
        $tabgeo = new TabGeo($this->filename);

        $this->assertTrue($tabgeo->country('8.8.8.8') === 'US');
    }
}
